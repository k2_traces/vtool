package main

import (
	"os"
	"os/exec"
	"path/filepath"
	// "fmt"
	"log"
)

func verifyError(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

// SplitToFrames cuts video into frames
func SplitToFrames(video string, dir string) error {
	cmdName := "ffmpeg"
	args := []string{
		"-i",
		video,
		dir + "/%d.png",
	}
	cmd := exec.Command(cmdName, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Start()
	verifyError(err)

	err = cmd.Wait()
	verifyError(err)

	return err
}

func listFiles(dir string) ([]string, error) {
	var files []string

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		if filepath.Ext(path) != ".mp4" {
			return nil
		}
		files = append(files, info.Name())
		return nil
	})

	if err != nil {
		return nil, err
	}

	return files, nil
}

func listFrames(dir string) ([]string, error) {
	var frames []string

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		if filepath.Ext(path) != ".png" {
			return nil
		}
		frames = append(frames, info.Name())
		return nil
	})

	if err != nil {
		return nil, err
	}

	return frames, nil
}
