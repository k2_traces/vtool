package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/urfave/cli/v2"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"strconv"
	"time"

	video "cloud.google.com/go/videointelligence/apiv1"
	vision "cloud.google.com/go/vision/apiv1"
	"github.com/golang/protobuf/ptypes"
	videopb "google.golang.org/genproto/googleapis/cloud/videointelligence/v1"
	visionpb "google.golang.org/genproto/googleapis/cloud/vision/v1"
)

// objectTrackingGCS analyzes a video and extracts entities with their bounding boxes.
func objectTrackingGCS(w io.Writer, file string) error {
	// gcsURI := "gs://cloud-samples-data/video/cat.mp4"

	ctx := context.Background()

	// Creates a client.
	client, err := video.NewClient(ctx)
	fileBytes, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	if err != nil {
		return fmt.Errorf("video.NewClient: %v", err)
	}
	op, err := client.AnnotateVideo(ctx, &videopb.AnnotateVideoRequest{
		Features: []videopb.Feature{
			videopb.Feature_OBJECT_TRACKING,
		},
		InputContent: fileBytes,
	})
	if err != nil {
		return fmt.Errorf("AnnotateVideo: %v", err)
	}
	resp, err := op.Wait(ctx)
	if err != nil {
		return fmt.Errorf("Wait: %v", err)
	}

	fmt.Print("Print results\n")

	// Only one video was processed, so get the first result.
	result := resp.GetAnnotationResults()[0]

	for _, annotation := range result.ObjectAnnotations {
		fmt.Fprintf(w, "Description: %q\n", annotation.Entity.GetDescription())
		if len(annotation.Entity.EntityId) > 0 {
			fmt.Fprintf(w, "\tEntity ID: %q\n", annotation.Entity.GetEntityId())
		}

		segment := annotation.GetSegment()
		start, _ := ptypes.Duration(segment.GetStartTimeOffset())
		end, _ := ptypes.Duration(segment.GetEndTimeOffset())
		fmt.Fprintf(w, "\tSegment: %v to %v\n", start, end)

		fmt.Fprintf(w, "\tConfidence: %f\n", annotation.GetConfidence())

		// Here we print only the bounding box of the first frame in this segment.
		frame := annotation.GetFrames()[0]
		seconds := float32(frame.GetTimeOffset().GetSeconds())
		nanos := float32(frame.GetTimeOffset().GetNanos())
		fmt.Fprintf(w, "\tTime offset of the first frame: %fs\n", seconds+nanos/1e9)

		box := frame.GetNormalizedBoundingBox()
		fmt.Fprintf(w, "\tBounding box position:\n")
		fmt.Fprintf(w, "\t\tleft  : %f\n", box.GetLeft())
		fmt.Fprintf(w, "\t\ttop   : %f\n", box.GetTop())
		fmt.Fprintf(w, "\t\tright : %f\n", box.GetRight())
		fmt.Fprintf(w, "\t\tbottom: %f\n", box.GetBottom())
	}

	return nil
}

func label(w io.Writer, file string) error {
	ctx := context.Background()
	client, err := video.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("video.NewClient: %v", err)
	}

	fileBytes, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	op, err := client.AnnotateVideo(ctx, &videopb.AnnotateVideoRequest{
		Features: []videopb.Feature{
			videopb.Feature_LABEL_DETECTION,
		},
		InputContent: fileBytes,
	})
	if err != nil {
		return fmt.Errorf("AnnotateVideo: %v", err)
	}

	resp, err := op.Wait(ctx)
	if err != nil {
		return fmt.Errorf("Wait: %v", err)
	}

	printLabels := func(labels []*videopb.LabelAnnotation) {
		for _, label := range labels {
			fmt.Fprintf(w, "\tDescription: %s\n", label.Entity.Description)
			for _, category := range label.CategoryEntities {
				fmt.Fprintf(w, "\t\tCategory: %s\n", category.Description)
			}
			for _, segment := range label.Segments {
				start, _ := ptypes.Duration(segment.Segment.StartTimeOffset)
				end, _ := ptypes.Duration(segment.Segment.EndTimeOffset)
				fmt.Fprintf(w, "\t\tSegment: %s to %s\n", start, end)
			}
		}
	}

	// A single video was processed. Get the first result.
	result := resp.AnnotationResults[0]

	fmt.Fprintln(w, "SegmentLabelAnnotations:")
	printLabels(result.SegmentLabelAnnotations)
	fmt.Fprintln(w, "ShotLabelAnnotations:")
	printLabels(result.ShotLabelAnnotations)
	fmt.Fprintln(w, "FrameLabelAnnotations:")
	printLabels(result.FrameLabelAnnotations)

	return nil
}

func filterAnnotations(annotations []*visionpb.LocalizedObjectAnnotation, confidence float32) []*visionpb.LocalizedObjectAnnotation {
	var ln int
	var name string
	var score float32
	for _, a := range annotations {
		name = a.GetName()
		score = a.GetScore()
		if ((name == "Person" || name == "Man" || name == "Woman") && score >= confidence) {
			annotations[ln] = a
			ln++
		}
	}
	return annotations[:ln]
}

// localizeObjects gets objects and bounding boxes from the Vision API for an image at the given file path.
func localizeObjects(file string) ([]*visionpb.LocalizedObjectAnnotation, error) {
	ctx := context.Background()

	client, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		return nil, err
	}
	defer client.Close()

	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	image, err := vision.NewImageFromReader(f)
	if err != nil {
		return nil, err
	}
	annotations, err := client.LocalizeObjects(ctx, image, nil)
	if err != nil {
		return nil, err
	}

	return annotations, nil
}

// AnnotationResult struct to be stored into JSON
type AnnotationResult struct {
	Total       int
	Annotations []*visionpb.LocalizedObjectAnnotation
}

func annotationsToJSON(annotations []*visionpb.LocalizedObjectAnnotation, path string) error {
	res := AnnotationResult{
		Total:       len(annotations),
		Annotations: annotations,
	}

	json, _ := json.MarshalIndent(res, "", " ")

	err := ioutil.WriteFile(path, json, 0644)
	if err != nil {
		return err
	}

	return nil
}

func saveTotal(path string, count int, ellapsed time.Duration) error {
	d := []byte(fmt.Sprintf("Total person detections: %v\nProcessing time: %v", count, ellapsed))
	err := ioutil.WriteFile(path, d, 0644)
	if err != nil {
		return err
	}

	return nil
}

func run(c *cli.Context) error {
	fmt.Print("Start\n")

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	files, err := listFiles(dir)
	if err != nil {
		log.Fatal(err)
	}

	var path, pathResults string
	var total int
	for _, file := range files {
		start := time.Now()
		total = 0
		log.Printf("File: %v\n", file)

		path = "./" + strings.TrimSuffix(file, filepath.Ext(file))
		pathResults = path + "/results"

		_, err := os.Stat(path)
		if os.IsNotExist(err) {
			os.Mkdir(path, os.ModePerm)

			log.Printf("Splitting file: %v", file)
			SplitToFrames(file, path)
		}

		err = os.RemoveAll(pathResults)
		if err != nil {
			log.Fatalf("Can't cleanup directory with JSON results. %v", err)
		}
		os.Mkdir(pathResults, os.ModePerm)

		frames, err := listFrames(path)
		if err != nil {
			log.Fatal(err)
		}
		for i := 0; i < len(frames); i += c.Int("step") {
			log.Printf("Detecting frame %v of %v from %v", i+1, len(frames), file)
			a, err := localizeObjects(path + "/" + frames[i])
			if err != nil {
				return err
			}

			annotations := filterAnnotations(a, float32(c.Float64("confidence")))
			if len(annotations) > 0 {
				log.Printf("%v objects found in frame %v", len(annotations), i+1)
				total += len(annotations)
				err = annotationsToJSON(annotations, pathResults+"/"+frames[i]+".json")
				if err != nil {
					return err
				}
			}
		}

		ellapsed := time.Since(start)
		err = saveTotal(pathResults+"/total_"+strconv.Itoa(total), total, ellapsed)
		if err != nil {
			return err
		}
	}

	if err != nil {
		return err
	}

	return nil
}

func main() {
	app := cli.NewApp()
	app.Name = "vtool"
	app.Usage = "command-line tool for people detecion in videostream, using Google Vision API"

	app.Flags = []cli.Flag{
		&cli.Float64Flag{
			Name:  "confidence",
			Value: 0.6,
			Usage: "Confidence threshold",
		},
		&cli.Int64Flag{
			Name:  "step",
			Value: 1,
			Usage: "Frame iteration step",
		},
	}

	app.Action = run

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
