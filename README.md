# vtool
Tool for people detection on videostream using Google Vision API

# Build

``` shell
$ make
```

# Run
``` shell
$ ./bin/vtool --help
```
