module github.com/r8d8/traces-edge

go 1.13

require (
	cloud.google.com/go v0.51.0
	github.com/golang/protobuf v1.3.2
	github.com/urfave/cli v1.22.2
	github.com/urfave/cli/v2 v2.1.1
	google.golang.org/genproto v0.0.0-20200113173426-e1de0a7b01eb
)
